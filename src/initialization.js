const bcrypt = require('bcrypt');

async function initialization(sl) {
    const databaseController = sl.get('DatabaseController');

    // await deleteTables(databaseController);

    const initNeeded = await checkInitializationNeeded(sl);

    if(!initNeeded) {
        console.log('Initialization skipped');
        return;
    }

    await _initialization(sl);
}

async function _initialization(sl) {
    const databaseController = sl.get('DatabaseController');
    const userRepository = sl.get('UserRepository');

    await createTables(databaseController);
    await createPermissions(sl);
    await createRoles(sl);
    await addPermissionsToRole(sl);

    const defaultPassword = process.env.INIT_DEFAULT_PASSWORD || generateDefaultPassword();

    const defaultAdmin = {
        username: 'admin',
        name: 'Administrator',
        role: 'admin',
        email: process.env.INIT_EMAIL || 'admin@example.com',
        hash: await bcrypt.hash(defaultPassword, 10)
    };

    userRepository.createUser(defaultAdmin);
}

async function checkInitializationNeeded(sl) {
    return new Promise(resolve => {
        const databaseController = sl.get('DatabaseController');
        databaseController
            .doesTableExist()
            .then((result) => {
                if(result[0].length === 0) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            })
            .catch(() => resolve(true));
    })
}

async function createTables(dbc) {
    const tableQuerys = [
        ''
    ];

    for (let i = 0; i < tableQuerys.length; i++) {
        await dbc.createTable(tableQuerys[i]);
    }
}

async function createPermissions(sl) {
    const permissionRepository = sl.get('PermissionRepository');

    const permissions = [

    ];

    for (let i = 0; i < permissions.length; i++) {
        await permissionRepository.createPermission(permissions[i]);
    }
}

async function createRoles(sl) {
    const roleRepository = sl.get('RoleRepository');

    const roles = [

    ];

    for (let i = 0; i < roles.length; i++) {
        await roleRepository.createRole(roles[i]);
    }
}

async function addPermissionsToRole(sl) {
    const roleRepository = sl.get('RoleRepository');

    const relations = [

    ];

    for (let i = 0; i < relations.length; i++) {
        await roleRepository.addPermissionToRole(relations[i]);
    }
}

async function deleteTables(databaseController) {
    const querys = [

    ];

    for (let i = 0; i < querys.length; i++) {
        databaseController.createTable(querys[i]);
    }
}

function generateDefaultPassword() {
    const password = Math.random().toString(36).slice(-10);
    console.log('###### PASSWORD #######');
    console.log('# You didnt specify a default password, so here is your new generated password: ');
    console.log(password);
    console.log('#######################');
    return password;
}

module.exports = {initialization};